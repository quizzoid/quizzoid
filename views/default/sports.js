
	$(function () {
		Array.prototype.shuffle = function (b) {
			var i = this.length, j, t;
			while (i) {
				j = Math.floor(( i-- ) * Math.random());
				t = b && typeof this[i].shuffle !== 'undefined' ? this[i].shuffle() : this[i];
				this[i] = this[j];
				this[j] = t;
			}

			return this;
		};
		

		var question = 0;
		var score = 0;
		var answered = false;
		var questions = [{"question":"Word stating with 'A'?","questionPicture":"","correct":0,"answers":["Apple","Orange","Elephant","All"]},
				{"question":"What does NASA stand for?","questionPicture":"","correct":0,"answers":["National Aeronautics and Space Administration","National Acadamy of Sea Agency","Nature ----------- ","None"]}];
        	var questionsPerQuiz = 1;

      /*  function loadXMLDoc()
        {
        	var xmlhttp;
        	if (window.XMLHttpRequest)
        	{
        		xmlhttp=new XMLHttpRequest();
        	}
        	else
        	{
        		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        	}
        	xmlhttp.onreadystatechange=function()
        	{
        		if (xmlhttp.readyState==4 && xmlhttp.status==200)
        		{
        				alert(xmlhttp.responseText);
        		}
        	}
        	xmlhttp.open("GET","http://127.0.0.1:8000/sachin/default/getsport",true);
        	xmlhttp.send();
        }*/
		function endGame()
		{
			$('.outer-wrapper').hide('fast');
			$('#showtime span').hide();
			$('.results').html('You answered  ' + score + '  of '+questions.length+' questions correctly!'
								+ '<br>' +
								'Your score is  ' + (score/questions.length*100|0) + '%.');

			//var $table = $('.table-results');
			//$table.find('.question-row:not(.row-template)').remove();
			if(score == questions.length)
			{
		//		$('.try-again span').text("Congratulations!!!") 
				$('.try-again span').html('Congratulations ' + '<br>' + 'You can proceed to Next Level')
			}
			else
			{
				$('.try-again span').text("Oops!!") 
			}
			$('.results-page').slideDown('slow');
		}

		function showNextQuestion(){
			showQuestion(++question);
		}

		function showQuestion(number){
			answered = false;
			if(!questions[number])
			{
				endGame();
			 	return;
			};
			$('.heading').text((number+1)+'/'+questions.length);
			question = number;
			$('.question-wrapper span').text(questions[number].question);
			/*if(questions[number].question) {
				$('.question-wrapper span').show();
			}
			else {
				$('.question-wrapper span').hide();
			}*/
			$('.explanation').text(questions[number].explanation);

            var random = [0,1,2,3].shuffle();

          $('.answers li').each(function(index,element){
				var $answer = $(element);
				$answer.removeClass('correct error apply')
					.attr('correct',!!(questions[number].correct==random[index]))
					.find('div').text(questions[number].answers[random[index]])
					.css({'font-size':'25px'});
				var $div = $answer.find('div');
				while( $div.height() > $answer.height() ) {
					$div.css('font-size', (parseInt($div.css('font-size')) - 1) + "px" );
				}
			});

			var count=10;

			var counter=setInterval(timer, 1000); //1000 will  run it every 1 second

			function timer()
			{
				var k;
			  count=count-1;
			  $('.answers li').click(function(){
					clearInterval(counter);
					//showNextQuestion();
			  })
			  if (count <= 0)
			  {
			     clearInterval(counter);
			     //counter ended, do something here
			     showNextQuestion();
			     return;
			  }
			  $('#showtime span').show();
			  document.getElementById("timer").innerHTML=count + " secs"; // watch for spelling
			  //Do code for showing the number of seconds here
			}
			$('#question-picture').attr("src", questions[number].questionPicture);
			if(!questions[number].questionPicture) {
				$('.picture-wrapper').hide();
			}
			else {
				$('.picture-wrapper').show();
				//$('#question-picture').height($('.answers').offset().top - $('.question-wrapper span').offset().top - $('.question-wrapper span').height() - 80);
			}					
		}

//		showQuestion(0);

		$('.answers li').click(function(){
			if(answered) 
				rbeturn;
			answered = true;
			var $answer = $(this);
			var correct = $answer.attr('correct')=='true';
			if(correct) 
				score++;
			questions[question].userCorrect = correct;
			questions[question].userAnswer = $answer.text();
			if(correct) {				
				$('#showtime span').hide();
				$('.answer-state').text('Your answer is correct!');

				$('.explanation').text("");
			}
			else {
				$('#showtime span').hide();
				$('.answer-state').text('Your answer is incorrect!');
                //$('.explanation').text("Correct answer is " + $('.answers li[correct=true]').text());
<!--				$('.explanation').text();-->
			}



            $answer.addClass(correct? 'correct' : 'error');
            $('.answers li[correct=true]').addClass('correct');
            $('.explanation-wrapper').removeClass('right wrong').addClass(correct ? 'right' : 'wrong').slideDown('medium');
            if(question == questions.length - 1) $('.next-question').val('Finish quiz');

		});


        function startGame(){
        	//loadXMLDoc();
            questions.shuffle();
            //alert(questions.length + " "+questionsPerQuiz);
          //  if(questions.length > questionsPerQuiz){
            //    questions.splice(questionsPerQuiz,questions.length-questionsPerQuiz);
          //  }
            //updateScore();
            showQuestion(0);
        }

        $('#start-game').click(function(){
            $('#intro_screen').hide("fast");
            $('#outer-wrapper').show("fast");
            startGame();

        });



		$('.next-question').click(function(){
			$('.explanation-wrapper').slideUp('fast');
			showNextQuestion();
		});

		$('.try-again').click(function(){
			question = 0;
			score = 0;
			answered = false;
			
			$('.intro_screen').show();
			$('.results-page').hide();
            
		});

	});

